var url = require('url');

exports.init = function(server){

  var self = this;
  
  var words = require('./words.json');

  var WebSocketServer = require('ws').Server;
  var wss = new WebSocketServer({
    server: server
  });
  
  self.wss = wss;

  var msg = function (type, data) {
    data.type = type;
    return JSON.stringify(data);
  };

  const MSG = {
    ERROR: 0,
    HANDSHAKE: 1,
    TEXT: 2,
    NAME: 3,
    TURN: 4,
    WORD: 5,
    WORD_FOUND: 6,
    DRAW: 7
  };

  wss.broadcast = function broadcast(data, exclude) {
    console.log('B',data);
    wss.clients.forEach(function each(client) {
      if (client.name && client !== exclude)
        client.send(data);
    });
  };

  setInterval(function () {
    game.checkState()
  }, 5000);

  var game = {
    active: false,
    drawing: undefined,
    word: undefined,
    timeout: undefined,
    endTime: undefined,
    turn: 0
  };

  self.game = game;
  
  var gameInterval = setInterval(function () {
    if(!game.endTime || game.endTime > Date.now())
      return;

    game.endTime = undefined;
    game.active = false;

    wss.broadcast((msg(MSG.TEXT, {
      text: "Time's Up! the word was \"" + game.word + "\""
    })));

  }, 1000);

  game.checkState = function () {
    if (!game.active && game.getActivePlayers().length >= 2) {
      var next = game.getNext();
      game.active = true;
      game.drawing = next;
      wss.broadcast(msg(MSG.TURN, {
        name: next.name
      }));
      game.word = words[~~(Math.random() * words.length)].toLowerCase();
      next.sendMessage(msg(MSG.WORD, {word: game.word}));
      game.endTime = Date.now() + 60000;

    } else if (game.active && game.getActivePlayers().length < 2) {
      game.active = false;
      wss.broadcast((msg(MSG.TEXT, {
        text: "Game is canceled, waiting for more players."
      })));
    }
  }

  game.getActivePlayers = function () {
    return wss.clients.filter(function (c) {
      return !c.terminating && c.name;
    });
  }

  game.getNext = function () {
    var activePlayers = game.getActivePlayers();
    activePlayers.sort(function (a, b) {
      return (a.turn > b.turn) ? 1 : ((b.turn > a.turn) ? -1 : 0);
    });
    activePlayers[0].turn = game.turn++;
    return activePlayers[0];
  };

  wss.on('connection', function connection(ws) {
    var location = url.parse(ws.upgradeReq.url, true);
    if(location.path!='/') {
      ws.terminate();
      return;
    }
    
    ws.sendMessage = function (message, callback) {
      if (ws.terminating)
        return;

      if (typeof message === 'object')
        message = JSON.stringify(message);

      ws.send(message, callback);
      console.log('>', message);
    };

    ws.terminateConnection = function (message) {
      ws.sendMessage(msg(MSG.ERROR, {
        text: message
      }), function () {
        ws.terminate();
      });
      ws.terminating = true;
      console.log('- terminating', ws.name, 'connection.');
    };

    ws.on('message', function incoming(data) {
      var message;
      try {
        message = JSON.parse(data);
        console.log('<', message);
      } catch (e) {
        console.log('<', typeof data, data);
        console.log('    ', e);
      }
      if (typeof message !== 'object') {
        console.log("    invalid message received", "terminating connection");
        ws.send(msg(MSG.ERROR, {
          text: "invalid message received"
        }), function () {
          ws.terminate();
        });
        return;
      }

      if (!ws.codeConfirmed) {
        if (ws.idCode !== message.idCode) {
          console.log("    confirmation not received", "terminating connection");
          ws.send("confirmation not received", function () {
            ws.terminate();
          });
        } else {
          ws.codeConfirmed = true;
        }
        return;
      }

      switch (message.type) {
        case MSG.TEXT:
          if (message.text.length > 100) {
            ws.terminateConnection("invalid message received");
            return;
          }
          if (game.active && message.text.toLowerCase().indexOf(game.word) !== -1) {
            wss.broadcast(msg(MSG.WORD_FOUND, {name: ws.name}));
            if (game.endTime > Date.now() + 5000) {
              game.endTime = Date.now() + 5000;
            }
          } else {
            wss.broadcast(msg(MSG.TEXT, {text: message.text, from: ws.name}), ws);
          }
          break;

        case MSG.NAME:
          if (ws.name === undefined) {
            ws.name = message.name;
            ws.sendMessage(msg(MSG.NAME, {name: message.name}));
            wss.broadcast(msg(MSG.TEXT, {text: ws.name + ' has joined.'}), ws);
            if(!game.active && game.getActivePlayers().length < 2) {
              ws.sendMessage(msg(MSG.TEXT, {text: "Waiting for more players."}));
            }
          } else {
            ws.terminateConnection("invalid message received");
          }
          break;

        case MSG.DRAW:
          if(game.drawing === ws)
            wss.broadcast(msg(MSG.DRAW, {points: message.points, color: message.color, line: message.line}), ws);
          break;

        default:
          console.log("    invalid message received", "terminating connection");
          ws.terminateConnection("invalid message received");
      }
    });

    ws.on('close', function () {
      console.log('client connection closed, ', wss.clients.length, 'clients remaining.');
    });

    ws.name = undefined;
    ws.turn = game.turn++;
    ws.idCode = Math.random().toString(36);
    ws.codeConfirmed = false;
    ws.sendMessage(msg(MSG.HANDSHAKE, {idCode: ws.idCode}));
  });

  return self;
};
