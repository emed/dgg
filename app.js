var server = require('http').createServer()
var fs = require('fs');
var dggServer = require('./server/server.js').init(server);
var express = require('express');
var app = express();

var ipaddress = process.env.IP || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;

app.get('/', function (req, res) {
  res.setHeader('Content-Type', 'text/html');
  res.send(fs.readFileSync('./client/index.html'));
});

app.get('/dgg.js', function (req, res) {
  res.setHeader('Content-Type', 'application/javascript');
  res.send(fs.readFileSync('./client/dgg.js'));
});

app.get('/_status', function (res, res) {
  res.setHeader('Content-Type', 'application/javascript');
  var status = {
    connections: dggServer.wss.clients.length,
    activeUsers: dggServer.game.getActivePlayers().length,
    endTime: dggServer.game.endTime,
    turn: dggServer.game.turn
  };
  res.send(JSON.stringify(status));
});

app.use(function (req, res) {
  res.status(404).end('Not Found');
});

server.on('request', app);
server.listen(port, ipaddress,
  function () { console.log('Listening on ' + server.address().port) });
