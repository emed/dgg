(function (dggApp) {
  'use strict';

  const MSG = {
    ERROR: 0,
    HANDSHAKE: 1,
    TEXT: 2,
    NAME: 3,
    TURN: 4,
    WORD: 5,
    WORD_FOUND: 6,
    DRAW: 7,
    CLEAR: 8
  };

  var msg = function (type, data) {
    data.type = type;
    return JSON.stringify(data);
  };

  dggApp.directive('scrollToBottom', function () {
    return function (scope, $element, attrs) {

      if (scope.$last) {
        //window.alert("im the last!");
        window.setTimeout(function () {
          var elem = document.getElementById('chatBox');
          elem.scrollTop = elem.scrollHeight;
        }, 50);
      }
    };
  });
  
  dggApp.factory('DggServerService', [dggServer]);
  
  function dggServer() {
    var self = this;
    var ws;

    self.onopen = undefined;
    self.onclose = undefined;
    self.onmessage = undefined;

    self.send = function (text) {
      if (typeof text === 'object')
        text = JSON.stringify(text);
      ws.send(text);
    };

    self.open = function () {
      ws = new WebSocket("ws://" + window.location.hostname);
      ws.onopen = self.onopen;
      ws.onclose = self.onclose;
      ws.onmessage = function (event) {
        try {
          var message = JSON.parse(event.data);
          self.onmessage(message);
        } catch (e) {
          console.error("Invalid message", event.data, e);
        }
      };
    };

    return self;
  }

  dggApp.controller('dggController', ['$scope', 'DggServerService', dggController]);

  function dggController($scope, dggServer) {
    var self = this;

    self.connected = false;
    self.active = false;
    self.myTurn = false;
    self.name = undefined;
    self.word = undefined;
    self.playerDrawing = undefined;
    self.timer = 0;
    self.messages = [];
    self.messageClasses = {
      0: 'error',
      2: 'text'
    };

    var timerInterval = setInterval(function () {
      if(!self.timer)
        return;
      
      $scope.$apply(function(){
        self.timer--;
        if (self.timer == 0) {
          self.myTurn = false;
          self.word = undefined;
          self.playerDrawing = undefined;
          self.clearCanvas();
        }
      });
    }, 1000);
    
    dggServer.onopen = function (event) {
      $scope.$apply(function(){
        self.connected = true;
      });
    };

    dggServer.onclose = function () {
      $scope.$apply(function(){
        self.connected = false;
        self.active = false;
      });
      addMessage({
        type: MSG.ERROR,
        text: "Connection closed."
      });
    };

    dggServer.onmessage = function (message) {
      console.log("<", message);
      switch (message.type) {
        case MSG.HANDSHAKE:
          dggServer.send(message);
          break;

        case MSG.NAME:
          $scope.$apply(function(){
            self.active = true;
            addMessage({type: MSG.TEXT, text: "Joined as " + message.name});
          });
          break;

        case MSG.TURN:
          $scope.$apply(function(){
            addMessage({type: MSG.TEXT, text: "It's " + message.name + " turn to draw!"});
            self.timer = 60;
            if (self.name === message.name) {
              self.myTurn = true;
            } else {
              self.playerDrawing = message.name;
            }
          });
          break;

        case MSG.WORD:
          $scope.$apply(function(){
            self.word = message.word;
          });
          break;

        case MSG.WORD_FOUND:
          if (self.timer > 5) {
            $scope.$apply(function(){
              self.timer = 5;
            });
          }
          addMessage({type: MSG.TEXT, text: message.name + " found the word!"});
          break;

        case MSG.ERROR:
        case MSG.TEXT:
          $scope.$apply(function () {
            addMessage(message);
          });
          break;
          
        case MSG.DRAW:
          var drawingPiece = {
            last: undefined,
            points: message.points,
            color: message.color,
            line: message.line,
            interval: null,
            draw: function() {
              if(this.points.length > 0) {
                draw(this.points[0], this.last, this.color, this.line);
                this.last = this.points.shift();
              } else {
                clearInterval(this.interval);
              }
            }
          };
          drawingPiece.interval = setInterval(function(){
            drawingPiece.draw.call(drawingPiece);
          }, 10);
          break;

        default:
          console.error("Invalid message", event.data);
      }
    };
	
	dggServer.onerror = function() {
		console.error('websocket error', arguments);
		addMessage({type: MSG.TEXT, text: 'Error conecting to server'});
	}

    dggServer.open();

    var canvas = document.getElementById('canvas');
    var context = canvas.getContext("2d");
    context.lineJoin = "round";
    var drawing = false;
    var lastPos;
    var positions = [];
    self.line = 1;
    self.color = '#000';
    canvas.onmousedown = function (e) {
      if (!self.myTurn)
        return;
      drawing = true;
      var pos = {
        x: e.offsetX,
        y: e.offsetY
      };
      positions.push(pos);
      draw(pos);
      lastPos = pos;
    };
    canvas.onmousemove = function (e) {
      if (drawing) {
        var pos = {
          x: e.offsetX,
          y: e.offsetY
        };
        positions.push(pos);
        draw(lastPos, pos);
        lastPos = pos;
      }
    };
    canvas.onmouseup = canvas.onmouseout = function (e) {
      drawing = false;
      sendDrawPiece();
    };
    var sendDrawPiece = function() {
      if(positions.length > 0) {
        var segment = positions.splice(0, positions.length);
        //positions.splice(0, segment.length);
        dggServer.send(msg(MSG.DRAW, {points: segment,
          color: self.color,
          line: self.line}));
      }
    };
    var sendDrawInterval = setInterval(function(){
      sendDrawPiece();
    }, 1000);
    var draw = function (start, end, color, line) {
      context.lineWidth = line || self.line || 1;
      context.strokeStyle = context.fillStyle = color || self.color || '#000';
      if (!end) {
        context.beginPath();
        context.arc(start.x, start.y, context.lineWidth > 1 ? context.lineWidth / 2 : 1, 0, 2 * Math.PI, true);
        context.fill();
      } else {
        context.beginPath();
        context.moveTo(start.x, start.y);
        context.lineTo(end.x, end.y);
        context.closePath();
        context.stroke();
      }
    }
    self.colors = [];
    var opt = ['0', '7', 'f'];
    for (var r = 0; r < opt.length; r++)
      for (var g = 0; g < opt.length; g++)
        for (var b = 0; b < opt.length; b++)
          self.colors.push(opt[r] + opt[g] + opt[b]);

    self.changeColor = function (color) {
      self.color = '#' + color;
    }
    self.changeLineWidth = function (size) {
      self.line = size;
    }
    self.clearCanvas = function () {
      context.clearRect(0, 0, context.canvas.width, context.canvas.height);
    }

    self.message = 'Guest-' + (~~(Math.random() * 1260) + 36).toString(36);
    self.name = undefined;

    self.sendMessage = function () {
      if (self.message && self.message.length <= 200) {
        if (self.active) {
          console.log(self.message.indexOf(self.word) !== -1);
          if (!(self.myTurn && self.message.indexOf(self.word) !== -1)) {

            addMessage({type: MSG.TEXT, text: self.message, from: self.name});
            dggServer.send(msg(MSG.TEXT, {text: self.message}));
          }
        } else {
          self.name = self.name = self.message;
          dggServer.send(msg(MSG.NAME, {name: self.message}));
        }
      }
      self.message = '';
    }

    function addMessage(message) {
      self.messages.push(message);
      if (self.messages.length > 50)
        self.messages.shift();
    }

    return self;
  }

})(angular.module('dggApp', []));